package dao;

import java.util.ArrayList;
import java.util.List;

public class ClassifierDao {

    public List<String> getCustomerTypes() {
        ArrayList<String> customerTypes = new ArrayList<>();
        customerTypes.add("customer_type.private");
        customerTypes.add("customer_type.corporate");
        return customerTypes;
    }

    public List<String> getPhoneTypes() {
        ArrayList<String> phoneTypes = new ArrayList<>();
        phoneTypes.add("phone_type.fixed");
        phoneTypes.add("phone_type.mobile");
        return phoneTypes;
    }
}
