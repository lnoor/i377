package controller;

import dao.CustomerDao;
import model.Customer;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CustomerController {

    @Resource
    private CustomerDao dao = new CustomerDao();

    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        return dao.getCustomers();
    }

    @GetMapping("/customers/{id}")
    protected Customer getOneCustomerById(@PathVariable Long id) {
        return dao.getCustomer(id);
    }

    @PostMapping("/customers")
    public void addNewCustomer(@RequestBody @Valid Customer customer) {
        dao.addCustomer(customer);
    }

    @DeleteMapping("/customers/{id}")
    public void deleteOne(@PathVariable Long id) {
        dao.deleteCustomer(id);
    }

    @DeleteMapping("/customers")
    protected void deleteAll() {
        dao.deleteCustomers();
    }

    @GetMapping("/customers/search")
    public List<Customer> searchCustomer(
            @RequestParam(defaultValue = "") String key) {
        return dao.getCustomerByKeyWord(key);
    }

}