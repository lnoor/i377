(function () {
    'use strict';

    var messages = {
        'NotNull.customer.firstName' : 'Customer firstname is missing',
        'NotNull.customer.lastName' : 'Customer lastname is missing',
        'NotNull.customer.code' : 'Customer code is missing',
        'Size.customer.firstName' : 'Firstname must be between {2} and {1} characters',
        'Size.customer.lastName' : 'Lastname must be between {2} and {1} characters',
        'Size.customer.code' : 'Code must be between {2} and {1} characters',
        'Pattern.customer.code' : 'Code must ONLY contain letters and numbers'
    };

    angular.module('app').service('messageService', Srv);

    function Srv() {

        this.getMessage = getMessage;

        function getMessage(key, params) {
            var text = messages[key];

            if (text === undefined) {
                return "Unknown message key: " + key;
            }

            if (params === undefined) {
                return text;
            }

            for (var i = 0; i < params.length; i++) {
                text = text.replace(new RegExp('\\{' + (i + 1) + '\\}', 'g'), params[i]);
            }

            return text;
        }

    }

})();
