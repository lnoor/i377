package dao;

import java.util.List;

import model.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CustomerDao {

    @PersistenceContext
    private EntityManager em;

    public List<Customer> getCustomers() {

        return em.createQuery("SELECT p FROM Customer p", Customer.class)
                .getResultList();

    }

    public Customer getCustomer(Long id) {

        return em.createQuery("SELECT p FROM Customer p WHERE p.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();

    }

    @Transactional
    public void addCustomer(Customer customer) {
        if (customer.getId() != null) {
            em.merge(customer);
        } else {
            em.persist(customer);
        }
    }

    @Transactional
    public void deleteCustomer(Long id) {
        em.createQuery("DELETE FROM Customer WHERE id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Transactional
    public void deleteCustomers() {
        em.createQuery("DELETE FROM Customer")
                .executeUpdate();
    }

    public List<Customer> getCustomerByKeyWord(String keyWord){
        keyWord = '%' + keyWord + '%';
        return em.createQuery("SELECT p FROM Customer p where LOWER(p.firstName) " +
                "like :firstName or LOWER(p.lastName) like :lastName or LOWER(p.code) like :code", Customer.class)
                .setParameter("firstName", keyWord)
                .setParameter("lastName", keyWord)
                .setParameter("code", keyWord)
                .getResultList();
    }

}
