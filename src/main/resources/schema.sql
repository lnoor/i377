DROP SCHEMA public CASCADE;

CREATE SEQUENCE seq1 START WITH 1;

CREATE TABLE customer (
       id BIGINT NOT NULL PRIMARY KEY,
       firstName VARCHAR(255),
       lastName VARCHAR(255),
       type VARCHAR(255),
       code VARCHAR(255)
);

CREATE TABLE phone (
       id BIGINT NOT NULL PRIMARY KEY,
       customer_id BIGINT,
       value VARCHAR(255),
       type VARCHAR (255),
       FOREIGN KEY (customer_id)
       REFERENCES customer
       ON DELETE RESTRICT
);

