(function () {
    'use strict';

    angular.module('app').controller('CustomerCtrl', Ctrl);

    function Ctrl(http, messageService) {

        var vm = this;
        vm.insertNew = insertNew;
        vm.deleteCustomer = deleteCustomer;

        vm.newCustomer = {};
        vm.customers = [];
        vm.errors = [];

        init();

        function init() {
            http.get('api/customers').then(function (data) {
                vm.customers = data;
                vm.errors = [];
            });
        }

        function insertNew() {
            http.post('api/customers', vm.newCustomer).then(function () {
                vm.newCustomer = {};
                init();
            }, errorHandler);
        }

        function deleteCustomer(customerId) {
            http.delete('api/customers/' + customerId).then(function () {
                init();
            }, errorHandler);
        }

        function errorHandler(response) {
            if (response.data.errors) {
                vm.errors = response.data.errors.map(function (error) {
                    return messageService.getMessage(error.code, error.arguments);
                });
            } else {
                console.log('Error: ' + JSON.stringify(response.data));
            }
        }
    }

})();
